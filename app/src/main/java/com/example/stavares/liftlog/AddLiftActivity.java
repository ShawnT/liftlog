package com.example.stavares.liftlog;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.stavares.liftlog.db.LiftsDataSource;
import com.example.stavares.liftlog.model.Lifts;

public class AddLiftActivity extends AppCompatActivity {
    Button submitButton, backButton;
    EditText liftText;
    Lifts lift;
    LiftsDataSource dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lift);

        submitButton = (Button) findViewById(R.id.add_lift_button);
        liftText = (EditText) findViewById(R.id.new_lift);
        dataSource = new LiftsDataSource(this);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataSource.open();
                String liftName = liftText.getText().toString();
                if (dataSource.findLiftNames().contains(liftName)){
                    Snackbar.make(v, liftName + " is already in the database", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }else {
                    lift = new Lifts();
                    lift.setName(liftName);
                    dataSource.create(lift);
                    Toast toast = Toast.makeText(getApplicationContext(), liftName + " has been added", Toast.LENGTH_SHORT);
                    toast.show();
                    dataSource.close();
                    finish();
                }
            }
        });
    }
}

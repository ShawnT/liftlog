package com.example.stavares.liftlog.model;

/**
 * Created by Shawn Tavares on 9/9/2016.
 */
public class Lifts {
    private long id;
    private String  name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

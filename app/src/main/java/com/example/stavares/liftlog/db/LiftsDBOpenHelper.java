package com.example.stavares.liftlog.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class LiftsDBOpenHelper extends SQLiteOpenHelper {

    private static final String LOGTAG = "LIFTLOG";

    private static final String DATABASE_NAME = "lifts.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_LIFTS = "lifts";
    public static final String LIFT_ID = "liftId";
    public static final String LIFT_NAME = "name";

    private static final String TABLE_LIFTS_CREATE =
            "CREATE TABLE " + TABLE_LIFTS + " (" +
                    LIFT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    LIFT_NAME + " TEXT " +
            ")";

    public static final String TABLE_EXERCISE = "exercises";
    public static final String EXERCISE_ID = "exerciseId";
    public static final String EXERCISE_LIFT = "exerciseLift";
    public static final String EXERCISE_DATE = "exerciseDate";

    private static final String TABLE_EXERCISE_CREATE =
            "CREATE TABLE " + TABLE_EXERCISE + " (" +
                    EXERCISE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    EXERCISE_LIFT + " INTEGER, " +
                    EXERCISE_DATE + " DATE, " +
                    "FOREIGN KEY(" + EXERCISE_LIFT + ") REFERENCES " +
                    TABLE_LIFTS + "(" + LIFT_ID + ")" +
                    ");";

    public static final String TABLE_SETS = "sets";
    public static final String SET_ID = "setId";
    public static final String SET_EXERCISE = "setExercise";
    public static final String SET_NUMBER_REPS = "setNumReps";
    public static final String SET_WEIGHT = "seTWeight";

    private static final String TABLE_SETS_CREATE =
            "CREATE TABLE " + TABLE_SETS + " (" +
                    SET_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    SET_EXERCISE + " INTERGER, " +
                    SET_NUMBER_REPS + " INTERGER, " +
                    SET_WEIGHT + "INTEGER," +
                    "FOREIGN KEY(" + SET_EXERCISE + ") REFERENCES " +
                    TABLE_EXERCISE + "(" + EXERCISE_ID + ")" +
                    ");";






    public LiftsDBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_LIFTS_CREATE);
        db.execSQL(TABLE_EXERCISE_CREATE);
        db.execSQL(TABLE_SETS_CREATE);
        Log.i(LOGTAG, "Table has been created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIFTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXERCISE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SETS);
        onCreate(db);
    }

    @Override
    public void onConfigure(SQLiteDatabase db){
        db.setForeignKeyConstraintsEnabled(true);
    }
}

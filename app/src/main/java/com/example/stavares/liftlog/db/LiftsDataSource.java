package com.example.stavares.liftlog.db;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.stavares.liftlog.model.Lifts;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class LiftsDataSource {

    private static final String LOGTAG = "LIFTLOG";

    SQLiteOpenHelper dbhelper;
    SQLiteDatabase database;

    private static final String[] liftNameColumns ={
            LiftsDBOpenHelper.LIFT_NAME
    };

    public LiftsDataSource(Context context){
        dbhelper = new LiftsDBOpenHelper(context);

    }

    public void open(){
        Log.i(LOGTAG, "Database opened");
        database = dbhelper.getWritableDatabase();
    }

    public void close(){
        Log.i(LOGTAG, "Database closed");
        dbhelper.close();
    }

    public Lifts create(Lifts lift) {
        ContentValues values = new ContentValues();
        values.put(LiftsDBOpenHelper.LIFT_NAME, lift.getName());
        long insertid = database.insert(LiftsDBOpenHelper.TABLE_LIFTS, null, values);
        lift.setId(insertid);
        return lift;
    }

    public List<String> findLiftNames(){
        List<String> names = new ArrayList<String>();

        Cursor cursor = database.query(LiftsDBOpenHelper.TABLE_LIFTS,liftNameColumns, null,null,null,null,null);

        if (cursor.getCount()>0)
            while (cursor.moveToNext()){
                String name = cursor.getString(cursor.getColumnIndex(LiftsDBOpenHelper.LIFT_NAME));
                names.add(name);
            }
        cursor.close();
        return names;
    }

}

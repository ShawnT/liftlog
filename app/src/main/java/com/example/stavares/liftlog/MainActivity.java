package com.example.stavares.liftlog;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.stavares.liftlog.db.LiftsDataSource;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    LiftsDataSource dataSource;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);






//        Spinner spinner = (Spinner) findViewById(R.id.lifts_spinner);
//        loadSpinnerData(spinner);

        final Count c = new Count();

        addLiftView(c);

                FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addLiftView(c);

                int n = c.getCount();
                Snackbar.make(view, "Something has happened" + n + "times", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

            }


        });



    }

    class Count{
        private int n = 0;

        public int getCount(){return n;}

        public void addCount() {n++;}

        private View vi=findViewById(R.id.main_scrollView);

        public void setView(View v){
            vi=v;
        }

        public View lastView(){
            return vi;
        }

    }


    private void addLiftView(Count c) {
        ViewGroup cLayout = (ViewGroup) c.lastView();
        View vi = getLayoutInflater().inflate(R.layout.lift_view, cLayout, false);

        View lSpinner = vi.findViewById(R.id.lifts_spinner);

        loadSpinnerData((Spinner) lSpinner);
        c.setView(vi);
        if (c.getCount()>0) {
           vi.setPadding(0,0,0,0);
        }
        c.addCount();
        cLayout.addView(vi);
    }




    private void loadSpinnerData(Spinner spinner) {
        dataSource = new LiftsDataSource(this);
        dataSource.open();
        List<String> names = dataSource.findLiftNames();
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, names);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.add_lift:
                Intent intent = new Intent(this, AddLiftActivity.class);
                startActivity(intent);

                break;

            case R.id.action_settings:

                break;

            default:
                break;
        }

//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume(){
        super.onResume();
        dataSource.open();
        loadSpinnerData((Spinner) findViewById(R.id.lifts_spinner));
    }

    @Override
    protected void onPause() {
        super.onPause();
        dataSource.close();
    }
}

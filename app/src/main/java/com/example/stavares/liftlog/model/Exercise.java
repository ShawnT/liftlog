package com.example.stavares.liftlog.model;

import java.util.Date;

/**
 * Created by Shawn Tavares on 9/9/2016.
 */
public class Exercise {
    private long id;
    private long liftId;
    private Date date;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLiftId() {
        return liftId;
    }

    public void setLiftId(long liftId) {
        this.liftId = liftId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
